DVD Manager
===========

Assumptions:
------------
 - You're using rvm with ruby-1.9.3-p194 [ x86_64 ] installed
 - You'll run this locally in development mode (production not currently supported)

Instructions:
-------------
 - git clone https://github.com/scotttam/dvd_manager.git
 - From the project root:
   - Accept the RVM question to create the dvd_manager gemset
   - gem install bundler
   - bundle install
   - rake db:setup
   - rake (to run all spec tests)
   - script/rails server
   - browse to: http://localhost:3000


Stories (Normally I'd put these in Pivotal Tracker, but for sake of simpleness, I'll just add them here)
--------------------------------------------------------------------------------------------------------

Required Stories
1. (Chore) Development environment setup

2. (Chore) Add the required models
  - Add DVD model with the following fields: name (required, unique), release date, summary (required), and ASIN (Amazon product ID).
  - Create Person model with the following fields: name (required, unique), date of birth, and gender (required). Use STI to:
    - Add Actor model - many to many relationship with dvd
    - Add Director model - many to one relationship with dvd
    - Add Casts model to facilitate the many_to_many relationship between actors and dvd

3. (Story) As the sole user of the system, I'd like to CRUD DVD's
  - When viewing a DVD, I should have a clickable link to the Amazon product page, if and only if an ASIN is present.

4. (Story) As the sole user of the system, I'd like to CRUD Actors

5. (Story) As the sole user of the system, I'd like to CRUD Directors

6. (Story) As the sole user of the system, I want to specify the actors/actresses and directors when adding/editing a dvd.
  - When adding/editing a DVD I must be able to specify the actors/actresses in the movie. (many-to-many relationship)
  - When adding/editing a DVD I must be able to specify the director of the movie. (many-to-one relationship)

7. (Story) As  the sole user of the system, I want to link movies to actors
  - When viewing an Actor, I must see a list of movies they have been in.
  - When editing an Actor, I must be able to add to/remove from the set of movies that they have been in.
  - When deleting an Actor, remove the join row from actors_dvds when the actor is deleted

Optional Stories

8. (Story) As the developer, I'd like an improved UI.

9. (Story) As the sole user of the system, When adding a DVD, I should be able add an actor/actress/director not already present in the system and have that record added to the system.

10. (Story) As the sole user of the system, I'd like to search for DVDs by name, director, or actor/actress.

11. (Story) As the sole user of the system, I should be able to search for actors by name, age range (not date of birth!), or movie (show a list of actors in that movie).

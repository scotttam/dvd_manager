require 'factory_girl_rails'

american_psycho = FactoryGirl.create(:american_psycho)
pulp_fiction = FactoryGirl.create(:pulp_fiction)
do_the_right_thing = FactoryGirl.create(:do_the_right_thing)

quentin = FactoryGirl.create(:quentin_tarantino)
mary = FactoryGirl.create(:mary_harron)

uma = FactoryGirl.create(:uma_therman)
samuel = FactoryGirl.create(:samuel_jackson)
christian = FactoryGirl.create(:christian_bale)

american_psycho.director = mary
american_psycho.actors << christian
american_psycho.save!

pulp_fiction.director = quentin
pulp_fiction.actors << uma
pulp_fiction.actors << samuel
pulp_fiction.save!

do_the_right_thing.actors << samuel
do_the_right_thing.save!
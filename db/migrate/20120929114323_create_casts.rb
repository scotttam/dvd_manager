class CreateCasts < ActiveRecord::Migration
  def change
    create_table :casts do |t|
      t.integer :actor_id
      t.integer :dvd_id
    end
  end
end

class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name
      t.string :gender
      t.string :type
      t.date :date_of_birth_at
      t.timestamps
    end
  end
end

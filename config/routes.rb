DvdManager::Application.routes.draw do
  
  resources :actors
  resources :directors
  resources :dvds
  
  root :to => 'dvds#index'
end

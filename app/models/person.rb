class Person < ActiveRecord::Base
  GENDERS = ['Male', 'Female']

  attr_accessible :date_of_birth_at, :gender, :name
  
  validates_presence_of :name, :gender
  validates_uniqueness_of :name  
end

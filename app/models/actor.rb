class Actor < Person
  attr_accessible :dvd_ids

  has_many :casts, :dependent => :destroy
  has_many :dvds, :through => :casts
end

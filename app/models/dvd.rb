class Dvd < ActiveRecord::Base
  attr_accessible :name, :release_date_at, :summary, :asin, :director_id, :actor_ids
  
  belongs_to :director
  has_many   :casts
  has_many   :actors, :through => :casts
  
  validates_presence_of :name, :summary
  validates_uniqueness_of :name
end

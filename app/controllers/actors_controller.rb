class ActorsController < ApplicationController
  respond_to :html
  
  before_filter :find_actor, :only => [:show, :edit, :update, :destroy]

  def index
    @actors = Actor.all
  end

  def show
  end

  def new
    @actor = Actor.new
  end

  def edit
  end

  def create
    @actor = Actor.new(params[:actor])
    
    if @actor.save
      redirect_to @actor, notice: 'Actor was successfully created.'
    else
      render action: 'new'
    end
  end

  def update
    if @actor.update_attributes(params[:actor])
      redirect_to @actor, notice: 'Actor was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @actor.destroy
    redirect_to actors_url, notice: 'Actor was successfully deleted.'
  end

  private

  def find_actor
    @actor = Actor.find_by_id(params[:id])
    redirect_to actors_url, :flash => {:error => 'Actor not found.'} unless @actor
  end
end

class DvdsController < ApplicationController
  respond_to :html

  before_filter :find_dvd, :only => [:show, :edit, :update, :destroy]

  def index
    @dvds = Dvd.all
  end

  def show
  end

  def new
    @dvd = Dvd.new
  end

  def edit
  end

  def create
    @dvd = Dvd.new(params[:dvd])

    if @dvd.save
      redirect_to @dvd, notice: 'Dvd was successfully created.'
    else
      render action: 'new'
    end
  end

  def update
    if @dvd.update_attributes(params[:dvd])
      redirect_to @dvd, notice: 'Dvd was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @dvd.destroy
    redirect_to dvds_url, notice: 'Dvd was successfully deleted.'
  end

  private

  def find_dvd
    @dvd = Dvd.find_by_id(params[:id])
    redirect_to dvds_url, :flash => {:error => 'Dvd not found.'} unless @dvd
  end
end

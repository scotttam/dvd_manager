class DirectorsController < ApplicationController
  respond_to :html

  before_filter :find_director, :only => [:show, :edit, :update, :destroy]

  def index
    @directors = Director.all
  end

  def show
  end

  def new
    @director = Director.new
  end

  def edit
  end

  def create
    @director = Director.new(params[:director])

    if @director.save
      redirect_to @director, notice: 'Director was successfully created.'
    else
      render action: 'new'
    end
  end

  def update
    if @director.update_attributes(params[:director])
      redirect_to @director, notice: 'Director was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @director.destroy
    redirect_to directors_url, notice: 'Director was successfully deleted.'
  end

  private

  def find_director
    @director = Director.find_by_id(params[:id])
    redirect_to directors_url, :flash => {:error => 'Director not found.'} unless @director
  end
end

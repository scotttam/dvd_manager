module DvdsHelper

  def action_links(dvd)
    [link_to('Delete', dvd_path(dvd), :confirm => 'Are you sure?', :method => :delete), link_to_amazon(dvd)].compact.join(" | ")
  end

  def link_to_amazon(dvd, link_title='View on Amazon')
    dvd.asin.blank? ? nil : link_to(link_title, "http://www.amazon.com/dp/#{dvd.asin}")
  end
end

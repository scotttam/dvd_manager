class Date

  def pretty_format
    strftime("%B %d, %Y")
  end
end
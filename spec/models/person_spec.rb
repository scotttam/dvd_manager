require 'spec_helper'

describe Person do
  describe "Validations" do
    context "Required fields" do
      subject do
        person = build(:person)
        person.valid?
        person.errors
      end

      its(:full_messages) { should == ["Name can't be blank", "Gender can't be blank"] }
    end

    context "Unique fields" do
      subject do
        create(:john_doe)
        dup_john_doe = build(:john_doe)
        dup_john_doe.valid?
        dup_john_doe.errors
      end

      its(:full_messages) { should == ["Name has already been taken"] }
    end

    context "Gender should be only 'Male' or 'Female'" do
      Person::GENDERS.each { |gender| it { should allow_value(gender).for(:gender) } }
    end
  end
end

require 'spec_helper'

describe Actor do
  context "Relationships" do
    it { should have_many(:dvds).through(:casts) }
    it { should have_many(:casts).dependent(:destroy) }
  end

  context "STI" do
    subject { create(:christian_bale) }

    its(:type) { should == "Actor" }
  end
end

require 'spec_helper'

describe Director do
  context "Relationships" do
    it { should have_many(:dvds) }
  end

  context "STI" do
    subject { create(:mary_harron) }

    its(:type) { should == "Director" }
  end
end
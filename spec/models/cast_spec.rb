require 'spec_helper'

describe Cast do
  context "Relationships" do
    it { should belong_to(:dvd) }
    it { should belong_to(:actor) }
  end
end
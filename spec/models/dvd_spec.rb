require 'spec_helper'

describe Dvd do
  context "Relationships" do
    it { should belong_to(:director) }
    it { should have_many(:actors).through(:casts) }
    it { should have_many(:casts) }
  end

  describe "Validations" do
    context "Required fields" do
      subject do
        dvd = build(:dvd)
        dvd.valid?
        dvd.errors
      end

      its(:full_messages) { should == ["Name can't be blank", "Summary can't be blank"] }
    end

    context "Unique fields" do
      subject do
        create(:american_psycho)
        dup_american_psycho = build(:american_psycho)
        dup_american_psycho.valid?
        dup_american_psycho.errors
      end

      its(:full_messages) { should == ["Name has already been taken"] }
    end
  end
end
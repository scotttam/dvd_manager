require 'spec_helper'

describe DvdsHelper do
  before(:each) do
    @american_psycho = create(:american_psycho)
    @pulp_fiction_no_asin = create(:pulp_fiction, :asin => '')
  end

  describe '#action_links' do
    it 'should have the delete link' do
      action_links(@american_psycho).should include("<a href=\"/dvds/1\" data-confirm=\"Are you sure?\" data-method=\"delete\" rel=\"nofollow\">Delete</a>")
    end

    it 'should have Amazon link if the asin field is not empty' do
      action_links(@american_psycho).should include("http://www.amazon.com/dp/#{@american_psycho.asin}")
    end

    it 'should not have the Amazon link if the asin field is empty' do
      action_links(@pulp_fiction_no_asin).should_not include('http://www.amazon.com/dp')
    end
  end

  describe "#link_to_amazon" do
    it 'should return a link to amazon' do
      link_to_amazon(@american_psycho).should include("http://www.amazon.com/dp/#{@american_psycho.asin}")
    end

    it 'should return nil if the asin is empty' do
      link_to_amazon(@pulp_fiction_no_asin).should be_nil
    end

    it 'should allow override of the link name' do
      link_to_amazon(@american_psycho, 'FOO BAR').should include('FOO BAR')
    end
  end
end

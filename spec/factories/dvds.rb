FactoryGirl.define do
  factory :dvd do
    factory :american_psycho do
      name             'American Psycho'
      release_date_at  DateTime.parse('April 14, 2000')
      summary          'Patrick Bateman (Christian Bale) is a young, handsome, Harvard-educated, Wall Street success, seemingly perfect with his stunning fiancee (Reese Witherspoon) and entourage of high-powered friends. But his circle of friends doesn\'t know the other Patrick Bateman, the one who lusts for women, wealth and the ultimate crime-murder! Based on the controversial book by Bret Easton Ellis, American Psycho is a sexy thriller that sets forth a vision that is both terrifying and chilling.'
      asin             'B00008RV1L'
    end

    factory :pulp_fiction do
      name             'Pulp Fiction'
      release_date_at  DateTime.parse('October 14, 1994')
      summary          'The lives of two mob hit men, a boxer, a gangster\'s wife, and a pair of diner bandits intertwine in four tales of violence and redemption.'
      asin             '1558908242'
    end

    factory :do_the_right_thing do
      name             'Do the Right Thing'
      release_date_at  DateTime.parse('June 30, 1989')
      summary          'On the hottest day of the year on a street in the Bedford-Stuyvesant section of Brooklyn, everyone\'s hate and bigotry smolders and builds until it explodes into violence.'
    end
  end
end

FactoryGirl.define do
  factory :person do
    factory :john_doe do
      name             'John Doe'
      gender           'Male'
      date_of_birth_at 25.years.ago
    end

    factory :actor, :class => 'Actor' do
      factory :christian_bale do
        name             'Christian Bale'
        gender           'Male'
        date_of_birth_at DateTime.parse('January 30, 1974')
      end

      factory :samuel_jackson do
        name             'Samuel L. Jackson'
        gender           'Male'
        date_of_birth_at DateTime.parse('December 21, 1948')
      end

      factory :uma_therman do
        name             'Uma Thurman'
        gender           'Female'
        date_of_birth_at DateTime.parse('April 29, 1970')
      end
    end

    factory :director, :class => 'Director' do
      factory :mary_harron do
        name             'Mary Harron'
        gender           'Female'
        date_of_birth_at DateTime.parse('January 12, 1953')
      end

      factory :quentin_tarantino do
        name             'Quentin Tarantino'
        gender           'Male'
        date_of_birth_at DateTime.parse('March 27, 1963')
      end
    end
  end
end

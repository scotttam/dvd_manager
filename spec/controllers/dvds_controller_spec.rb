require 'spec_helper'

describe DvdsController do
  describe 'GET #index' do
    before(:each) do
      @american_psycho = create(:american_psycho)
      @pulp_fiction = create(:pulp_fiction)
      get :index
    end

    it { response.should be_success }
    it { response.should render_template(:index) }
    it { assigns(:dvds).should == [@american_psycho, @pulp_fiction] }
  end

  describe 'GET #show' do
    context 'success' do
      before(:each) do
        @pulp_fiction = create(:pulp_fiction)
        get :show, :id => @pulp_fiction.id
      end

      it { response.should be_success }
      it { response.should render_template(:show) }
      it { assigns(:dvd).should == @pulp_fiction }
    end

    context 'not found' do
      before(:each) { get :show, :id => 123456 }

      it { response.should redirect_to(dvds_url) }
      it { flash[:error].should == 'Dvd not found.' }
    end
  end

  describe 'GET #new' do
    before(:each) { get :new }

    it { response.should be_success }
    it { response.should render_template(:new) }
    it { assigns(:dvd).should be_a_new(Dvd) }
  end

  describe 'POST #create' do
    context 'success' do
      before(:each) do
        lambda do
          post :create, dvd: {name: 'Lock, Stock and Two Smoking Barrels', release_date_at: 'March 5th, 1999', asin: '630549228X',
                              summary: 'Four London working class stiffs pool their money to put one in a high stakes card game, but things go wrong and they end up owing half a million pounds and having one week to come up with the cash.'}
        end.should change(Dvd, :count).by(1)
      end

      it { assigns(:dvd).should be_a(Dvd) }
      it { assigns(:dvd).should be_persisted }
      it { response.should redirect_to(dvd_url(Dvd.first)) }
    end

    context 'failure on save' do
      before(:each) do
        lambda do
          post :create, dvd: {name: '', release_date_at: 'March 5th, 1999', asin: '630549228X',
                              summary: 'Four London working class stiffs pool their money to put one in a high stakes card game, but things go wrong and they end up owing half a million pounds and having one week to come up with the cash.'}
        end.should_not change(Dvd, :count)
      end

      it { assigns(:dvd).should be_a_new(Dvd) }
      it { response.should render_template(:new) }
    end
  end

  describe 'GET #edit' do
    context 'should render the edit page' do
      before(:each) do
        @american_psycho = create(:american_psycho)
        get :edit, :id => @american_psycho.id
      end

      it { response.should be_success }
      it { response.should render_template(:edit) }
      it { assigns(:dvd).should == @american_psycho }
    end

    context 'not found' do
      before(:each) { get :edit, :id => 123456 }

      it { response.should redirect_to(dvds_url) }
      it { flash[:error].should == 'Dvd not found.' }
    end
  end

  describe 'PUT #update' do
    context 'success' do
      before(:each) do
        @american_psycho = create(:american_psycho)
        put :update, :id => @american_psycho.id, :dvd => {:name => @new_name = 'American Psycho Two, Electric Boogaloo'}
      end

      it { response.should redirect_to(dvd_url(@american_psycho)) }
      it { @american_psycho.reload.name.should == @new_name }
    end

    context 'not found' do
      before(:each) { put :update, :id => 123456 }

      it { response.should redirect_to(dvds_url) }
      it { flash[:error].should == 'Dvd not found.' }
    end

    context 'failure on save' do
      before(:each) do
        @american_psycho = create(:american_psycho)
        put :update, :id => @american_psycho.id, :dvd => {:name => ''}
      end

      it { response.should render_template(:edit) }
      it { @american_psycho.reload.name.should == 'American Psycho' }
    end
  end

  describe 'DELETE #destroy' do
    context 'success' do
      before(:each) do
        @american_psycho = create(:american_psycho)
        lambda { delete :destroy, :id => @american_psycho.id }.should change(Dvd, :count).by(-1)
      end

      it { response.should redirect_to(dvds_url) }
    end

    context 'not found' do
      before(:each) { delete :destroy, :id => 123456 }

      it { response.should redirect_to(dvds_url) }
      it { flash[:error].should == 'Dvd not found.' }
    end
  end
end

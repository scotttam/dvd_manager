require 'spec_helper'

describe ActorsController do

  describe 'GET #index' do
    before(:each) do
      @uma_therman = create(:uma_therman)
      @samuel_jackson = create(:samuel_jackson)
      get :index
    end

    it { response.should be_success }
    it { response.should render_template(:index) }
    it { assigns(:actors).should == [@uma_therman, @samuel_jackson] }
  end

  describe 'GET #show' do
    context 'success' do
      before(:each) do
        @uma_therman = create(:uma_therman)
        get :show, :id => @uma_therman.id
      end

      it { response.should be_success }
      it { response.should render_template(:show) }
      it { assigns(:actor).should == @uma_therman }
    end

    context 'not found' do
      before(:each) { get :show, :id => 123456 }

      it { response.should redirect_to(actors_url) }
      it { flash[:error].should == 'Actor not found.' }
    end
  end

  describe 'GET #new' do
    before(:each) { get :new }

    it { response.should be_success }
    it { response.should render_template(:new) }
    it { assigns(:actor).should be_a_new(Actor) }
  end

  describe 'POST #create' do
    context 'success' do
      before(:each) do
        lambda do
          post :create, actor: { name: 'Tom Hanks', gender: 'Male', date_of_birth_at: 'July 9, 1956' }
        end.should change(Actor, :count).by(1)
      end

      it { assigns(:actor).should be_a(Actor) }
      it { assigns(:actor).should be_persisted }
      it { response.should redirect_to(actor_url(Actor.first)) }
    end

    context 'failure on save' do
      before(:each) do
        lambda do
          post :create, actor: { name: '', gender: 'Male', date_of_birth_at: 'July 9, 1956' }
        end.should_not change(Actor, :count)
      end

      it { assigns(:actor).should be_a_new(Actor) }
      it { response.should render_template(:new) }
    end
  end

  describe 'GET #edit' do
    context 'should render the edit page' do
      before(:each) do
        @uma_therman = create(:uma_therman)
        get :edit, :id => @uma_therman.id
      end

      it { response.should be_success }
      it { response.should render_template(:edit) }
      it { assigns(:actor).should == @uma_therman }
    end

    context 'not found' do
      before(:each) { get :edit, :id => 123456 }

      it { response.should redirect_to(actors_url) }
      it { flash[:error].should == 'Actor not found.' }
    end
  end

  describe 'PUT #update' do
    context 'success' do
      before(:each) do
        @uma_therman = create(:uma_therman)
        put :update, :id => @uma_therman.id, :actor => {:name => @new_name = 'Uma Thermaner'}
      end

      it { response.should redirect_to(actor_url(@uma_therman)) }
      it { @uma_therman.reload.name.should == @new_name }
    end

    context 'not found' do
      before(:each) { put :update, :id => 123456 }

      it { response.should redirect_to(actors_url) }
      it { flash[:error].should == 'Actor not found.' }
    end

    context 'failure on save' do
      before(:each) do
        @uma_therman = create(:uma_therman)
        put :update, :id => @uma_therman.id, :actor => {:name => ''}
      end

      it { response.should render_template(:edit) }
      it { @uma_therman.reload.name.should == 'Uma Thurman' }
    end
  end

  describe 'DELETE #destroy' do
    context 'success' do
      before(:each) do
        @uma_therman = create(:uma_therman)
        lambda { delete :destroy, :id => @uma_therman.id }.should change(Actor, :count).by(-1)
      end

      it { response.should redirect_to(actors_url) }
    end

    context 'not found' do
      before(:each) { delete :destroy, :id => 123456 }

      it { response.should redirect_to(actors_url) }
      it { flash[:error].should == 'Actor not found.' }
    end
  end
end

require 'spec_helper'

describe DirectorsController do

  describe 'GET #index' do
    before(:each) do
      @quentin_tarantino = create(:quentin_tarantino)
      @mary_harron = create(:mary_harron)
      get :index
    end

    it { response.should be_success }
    it { response.should render_template(:index) }
    it { assigns(:directors).should == [@quentin_tarantino, @mary_harron] }
  end

  describe 'GET #show' do
    context 'success' do
      before(:each) do
        @quentin_tarantino = create(:quentin_tarantino)
        get :show, :id => @quentin_tarantino.id
      end

      it { response.should be_success }
      it { response.should render_template(:show) }
      it { assigns(:director).should == @quentin_tarantino }
    end

    context 'not found' do
      before(:each) { get :show, :id => 123456 }

      it { response.should redirect_to(directors_url) }
      it { flash[:error].should == 'Director not found.' }
    end
  end

  describe 'GET #new' do
    before(:each) { get :new }

    it { response.should be_success }
    it { response.should render_template(:new) }
    it { assigns(:director).should be_a_new(Director) }
  end

  describe 'POST #create' do
    context 'success' do
      before(:each) do
        lambda do
          post :create, director: { name: 'Tom Hanks', gender: 'Male', date_of_birth_at: 'July 9, 1956' }
        end.should change(Director, :count).by(1)
      end

      it { assigns(:director).should be_a(Director) }
      it { assigns(:director).should be_persisted }
      it { response.should redirect_to(director_url(Director.first)) }
    end

    context 'failure on save' do
      before(:each) do
        lambda do
          post :create, director: { name: '', gender: 'Male', date_of_birth_at: 'July 9, 1956' }
        end.should_not change(Director, :count)
      end

      it { assigns(:director).should be_a_new(Director) }
      it { response.should render_template(:new) }
    end
  end

  describe 'GET #edit' do
    context 'should render the edit page' do
      before(:each) do
        @quentin_tarantino = create(:quentin_tarantino)
        get :edit, :id => @quentin_tarantino.id
      end

      it { response.should be_success }
      it { response.should render_template(:edit) }
      it { assigns(:director).should == @quentin_tarantino }
    end

    context 'not found' do
      before(:each) { get :edit, :id => 123456 }

      it { response.should redirect_to(directors_url) }
      it { flash[:error].should == 'Director not found.' }
    end
  end

  describe 'PUT #update' do
    context 'success' do
      before(:each) do
        @quentin_tarantino = create(:quentin_tarantino)
        put :update, :id => @quentin_tarantino.id, :director => {:name => @new_name = 'Uma Thermaner'}
      end

      it { response.should redirect_to(director_url(@quentin_tarantino)) }
      it { @quentin_tarantino.reload.name.should == @new_name }
    end

    context 'not found' do
      before(:each) { put :update, :id => 123456 }

      it { response.should redirect_to(directors_url) }
      it { flash[:error].should == 'Director not found.' }
    end

    context 'failure on save' do
      before(:each) do
        @quentin_tarantino = create(:quentin_tarantino)
        put :update, :id => @quentin_tarantino.id, :director => {:name => ''}
      end

      it { response.should render_template(:edit) }
      it { @quentin_tarantino.reload.name.should == 'Quentin Tarantino' }
    end
  end

  describe 'DELETE #destroy' do
    context 'success' do
      before(:each) do
        @quentin_tarantino = create(:quentin_tarantino)
        lambda { delete :destroy, :id => @quentin_tarantino.id }.should change(Director, :count).by(-1)
      end

      it { response.should redirect_to(directors_url) }
    end

    context 'not found' do
      before(:each) { delete :destroy, :id => 123456 }

      it { response.should redirect_to(directors_url) }
      it { flash[:error].should == 'Director not found.' }
    end
  end
end
